package pizza.spring.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;

public class CommandPage {

    private WebDriver webDriver;

    public CommandPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        assertFalse("Cette page n'est pas la page de commande", webDriver.findElements(By.id("commande")).isEmpty());
    }

    public CommandPage choosePizza(String pizzaName) {
        Select dropdown = new Select(webDriver.findElement(By.id("pizzaId")));
        dropdown.selectByVisibleText(pizzaName);
        return this;
    }

    public CommandPage enterName(String name) {
        WebElement nameInput = webDriver.findElement(By.name("nom"));
        nameInput.sendKeys(name);
        return this;
    }

    public CommandPage enterTelephone(String telephone) {
        WebElement telephoneInput = webDriver.findElement(By.name("telephone"));
        telephoneInput.sendKeys(telephone);
        return this;
    }

    public RecapCommandPage clickOnCommander() {
        WebElement commanderButton = webDriver.findElement(By.cssSelector("button"));
        commanderButton.click();
        return new RecapCommandPage(webDriver);
    }
    public CommandPage clickOnCommanderErreur() {
        WebElement commanderButton = webDriver.findElement(By.cssSelector("button"));
        commanderButton.click();
        return this;
    }

    public boolean isMessageDErreurTelephone(){
        return ! webDriver.findElements(By.id("telephone.errors")).isEmpty();
    }

    public CommandPage waitASecond(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public boolean isMessageDErreurPizza(){
        return ! webDriver.findElements(By.id("pizzaId.errors")).isEmpty();
    }
}
