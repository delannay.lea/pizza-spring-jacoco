package pizza.spring.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class CommanderUnePizzaIntegrationTest {

    private WebDriver webDriver;

    @Before
    public void createWebDriver() {
        webDriver = new ChromeDriver();
        this.webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @After
    public void closeWebDriver() {
        webDriver.quit();
    }

    @Test
    public void commanderUnePizzaAvecSucces() throws Exception {
        RecapCommandPage recapCommandPage = HomePage.openWith(webDriver)
                .clickOnCommand()
                .choosePizza("Regina")
                .waitASecond(1000)
                .enterName("Alex")
                .waitASecond(1000)
                .enterTelephone("0601010101")
                .clickOnCommander();

        assertTrue(recapCommandPage.isPageRecapCommande());
    }

    @Test
    public void commanderUnePizzaSansNumeroDeTelephone() throws Exception {
        CommandPage commandPage = HomePage.openWith(webDriver)
                .clickOnCommand()
                .choosePizza("Regina")
                .waitASecond(1000)
                .enterName("Alex")
                .clickOnCommanderErreur();

        assertTrue(commandPage.isMessageDErreurTelephone());
    }

    @Test
    public void commanderUnePizzaSansPizza() throws Exception {
        CommandPage commandPage = HomePage.openWith(webDriver)
                .clickOnCommand()
                .enterName("Alex")
                .waitASecond(1000)
                .enterTelephone("0601010101")
                .clickOnCommanderErreur();

        assertTrue(commandPage.isMessageDErreurPizza());
    }
}

