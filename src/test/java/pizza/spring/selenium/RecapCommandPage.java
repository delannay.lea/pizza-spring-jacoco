package pizza.spring.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertFalse;

public class RecapCommandPage {

    private WebDriver webDriver;

    public RecapCommandPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        assertFalse("Cette page n'est pas la page de récap de commande", webDriver.findElements(By.id("recap-commande")).isEmpty());
    }

    public boolean isPageRecapCommande(){
        return !webDriver.findElements(By.id("recap-commande")).isEmpty();
    }
}
